# java-web-apps

A collection of different types of java applications to test sast rules. For new application types, please create a directory in this root folder.

For adding new tests, see the individual projects and their README.md for details.


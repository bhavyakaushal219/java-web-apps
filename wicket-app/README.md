## apache wicket

### Running

Run:

```
mvn package
docker build -t wicket-app . && docker run --rm -p 8080:8080 wicket-app
# Once started, navigate to the following endpoint, and to test xss, inject payload at 'input' url parameter.
"http://localhost:8080/?input=hi<h2>anything</h2><script>console.log(1);</script>"
```

Rule Ref: java_xss_rule-WicketXSS
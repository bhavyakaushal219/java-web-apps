## jakarta.servlet

Used for testing web applications source/sinks based on the newer jakarta.servlet namespace.

### Adding tests

Simply create a new package or find the applicable package under `src/main/java/com/gitlab/<type>` to create the new class.
Create the class that demonstrates the vulnerability.

### Running

Run:

```
mvn package
docker build -t jakartasample . && docker run --rm -p 8080:8080 --name jakartasample jakartasample
# Once started, hit the endpoint, all servlets are under the "/Servlets-Sample" application namespace.
curl -vvv "http://localhost:8080/Servlets-Sample/TestInject?input=test"
```

For testing CookieHttpOnly class:
(Rule: java_cookie_rule-CookieHTTPOnly)

```
Case 1: http://localhost:8080/Servlets-Sample/CookieHttpOnly?input=danger
Case 2: http://localhost:8080/Servlets-Sample/CookieHttpOnly?input=danger2
Case 3: http://localhost:8080/Servlets-Sample/CookieHttpOnly?input=safe
```

For testing CookieInsecure class:
(Rule: java_cookie_rule-CookieInsecure)

```
Case 1: http://localhost:8080/Servlets-Sample/CookieInsecure?input=danger
Case 2: http://localhost:8080/Servlets-Sample/CookieInsecure?input=danger2
Case 3: http://localhost:8080/Servlets-Sample/CookieInsecure?input=safe
```

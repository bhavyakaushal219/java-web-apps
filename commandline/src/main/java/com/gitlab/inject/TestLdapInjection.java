package com.gitlab.inject;

import com.gitlab.util.ScenarioRunner;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.util.Properties;
import java.util.Scanner;

//ref: java_inject_rule-LDAPInjection
public class TestLdapInjection implements ScenarioRunner {

    /*
    Pre-requisites:
    Run a test instance of LDAP with the following docker command.

    docker run --rm --name openldap \
        -p 3889:1389 \
        --env LDAP_ORGANISATION="example" \
        --env LDAP_DOMAIN="org" \
        --env LDAP_ADMIN_USERNAME=admin \
        --env LDAP_ADMIN_PASSWORD=adminpassword \
        --env LDAP_USERS=customuser \
        --env LDAP_PASSWORDS=custompassword \
        bitnami/openldap:latest
    */

    public void initiateLdapTests() throws NamingException {
        Properties props = setupEnvironment();
        InitialDirContext context = new InitialDirContext(props);

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter LDAP search filter (e.g., (objectClass=person)): ");
        String searchFilterOne = scanner.nextLine();
        processSearchResult(queryUsingDirectUserInput(context, searchFilterOne));

        System.out.print("Enter LDAP search filter (e.g., person): ");
        String searchFilterTwo = scanner.nextLine();
        processSearchResult(queryUsingConcatUserInput(context, searchFilterTwo));

        System.out.print("Enter LDAP search filter (e.g., customuser): ");
        String searchFilterThree = scanner.nextLine();
        processSearchResult(queryUsingEncodedUserInput(context, searchFilterThree));

        scanner.close();
        closeConnection(context);
    }

    public Properties setupEnvironment() {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://127.0.0.1:3889");
        props.put(Context.SECURITY_AUTHENTICATION, "simple");
        props.put(Context.SECURITY_PRINCIPAL, "cn=admin,dc=example,dc=org");//adminuser - User with special priviledge, dn user
        props.put(Context.SECURITY_CREDENTIALS, "adminpassword");//dn user password

        return props;
    }

    public NamingEnumeration<SearchResult> queryUsingDirectUserInput(InitialDirContext context, String input) {
        try {
            // Specify the search base
            String searchBase = "dc=example,dc=org";
            // Perform the search
            // ruleid: java_inject_rule-LDAPInjection
            return context.search(searchBase, input, createSearchControls());
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    public NamingEnumeration<SearchResult> queryUsingConcatUserInput(InitialDirContext context, String input) {
        try {
            //Specify the filter
            String filter = "(objectClass=" + input +")";
            // Specify the search base
            String searchBase = "dc=example,dc=org";
            // Perform the search
            // ruleid: java_inject_rule-LDAPInjection
            return context.search(searchBase, filter, createSearchControls());
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    public NamingEnumeration<SearchResult> queryUsingEncodedUserInput(InitialDirContext context, String input) {
        try {
            System.out.println(encodeLDAPString(input));
            //Specify the filter
            String filter = "(uid="+encodeLDAPString(input)+")";
            // Specify the search base
            String searchBase = "dc=example,dc=org";
            // Perform the search
            // ok: java_inject_rule-LDAPInjection
            return context.search(searchBase, filter, createSearchControls());
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    public String encodeLDAPString(String input) {
        // Note the \ character is replaced first
        CharSequence[] chars = new CharSequence[] { "\\", "\0", "(", ")", "*" };
        CharSequence[] encoded = new CharSequence[] { "\\5c", "\\00", "\\28", "\\29", "\\2a" };
        // Iterate over each character sequence, replacing the raw value with an encoded version of it
        for (int i = 0; i < chars.length; i++)
        {
            // re-assign to input
            input = input.replace(chars[i], encoded[i]);
        }
        // return our modified input string
        return input;
    }

    public void processSearchResult(NamingEnumeration<SearchResult> results) throws NamingException {
        while (results.hasMore()) {
            SearchResult result = results.next();
            Attributes attrs = result.getAttributes();
            System.out.println("DN: " + result.getNameInNamespace());
            NamingEnumeration<? extends Attribute> attrEnum = attrs.getAll();
            while (attrEnum.hasMore()) {
                Attribute attribute = attrEnum.next();
                System.out.println(attribute.getID() + ": " + attribute.get());
            }
            System.out.println("----------------------");
        }
    }

    public SearchControls createSearchControls() {
        // Create the search controls
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        return searchControls;
    }

    public void closeConnection(InitialDirContext context) {
        try {
            context.close();
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        try {
            initiateLdapTests();
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }
}

package com.gitlab.saml;

import org.apache.log4j.BasicConfigurator;
import org.opensaml.xml.parse.BasicParserPool;

import com.gitlab.util.ScenarioRunner;

// For TestSamlIgnoreComments, use the jar-with-dependencies. See README.md
// Rule ref: java_xml_rule-SAMLIgnoreComments
public class TestSamlIgnoreComments implements ScenarioRunner {

    void parserPool() {
        BasicParserPool pool = new BasicParserPool();
        // ruleid: java_xml_rule-SAMLIgnoreComments
        pool.setIgnoreComments(false);
        System.out.println("Called 1st unsafe method");
    }

    void parserPool2() {
        boolean shouldIgnore = false;
        BasicParserPool pool = new BasicParserPool();
        // ruleid: java_xml_rule-SAMLIgnoreComments
        pool.setIgnoreComments(shouldIgnore);
        System.out.println("Called 2nd unsafe method");
    }

    @Override
    public void run() throws Exception {
        BasicConfigurator.configure();
        parserPool();
        parserPool2();
    }
}

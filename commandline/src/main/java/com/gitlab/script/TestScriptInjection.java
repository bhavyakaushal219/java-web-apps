package com.gitlab.script;

import com.gitlab.util.ScenarioRunner;

import javax.script.*;
import java.io.*;
import java.nio.charset.StandardCharsets;

// ref: java_script_rule-ScriptInjection
public class TestScriptInjection implements ScenarioRunner {

    public static void scriptingUnsafe(String userInput) throws ScriptException {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        // Create a JavaScript engine
        ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("javascript");
        // ruleid: java_script_rule-ScriptInjection
        Object result = scriptEngine.eval(userInput);
        System.out.println(result);
    }

    public static void scriptingSafe() throws ScriptException {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        ScriptEngine engine = scriptEngineManager.getEngineByName("javascript");
        String code = "var x = 10; x * 2;";
        // ok: java_script_rule-ScriptInjection
        Object result = engine.eval(code);
        System.out.println(result);
    }

    public void evalWithBindings(String userFirstName, String userLastName) throws ScriptException {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        // Create bindings to pass into our script, forcing the values to be String.
        Bindings bindings = engine.createBindings();
        bindings.put("fname", new String(userFirstName));
        bindings.put("lname", new String(userLastName));
        // Example script that concatenates a greeting with the user-supplied input first/last name
        String script = "var greeting='Hello ';" +
                // fname and lname variables will be resolved by bindings defined above
                "greeting += fname + ' ' + lname;" +
                // prints greeting
                "greeting";
        // ok: java_script_rule-ScriptInjection
        Object result = engine.eval(script, bindings);
        System.out.println(result);
    }

    public void invokeFunctionUsage(String input) throws Exception {
        ScriptEngineManager engineManager = new ScriptEngineManager();
        ScriptEngine engine = engineManager.getEngineByName("javascript");
        // Load the JavaScript file from the classpath
        InputStream inputStream = TestScriptInjection.class.getClassLoader().getResourceAsStream("scriptInjectionTest.js");
        if (inputStream == null) {
            throw new Exception("Script file not found in classpath");
        }
        // Read the content of the file
        InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        // Evaluate the JavaScript code
        engine.eval(reader);

        // Check if the engine supports the Invocable interface
        if (engine instanceof Invocable) {
            Invocable invocable = (Invocable) engine;
            // Invoke the "greet" function from the JavaScript file
            // ok: java_script_rule-ScriptInjection
            String resultOne = (String) invocable.invokeFunction("greetOne", "John");
            // ruleid: java_script_rule-ScriptInjection
            String resultTwo = (String) invocable.invokeFunction("greetOne", input);
            // Display the result
            System.out.println(resultOne);
            System.out.println(resultTwo);
        }
    }

    public void invokeMethodUsage(String input) throws Exception {
        ScriptEngineManager engineManager = new ScriptEngineManager();
        ScriptEngine engine = engineManager.getEngineByName("javascript");
        InputStream inputStream = TestScriptInjection.class.getClassLoader().getResourceAsStream("scriptInjectionTest.js");
        if (inputStream == null) {
            throw new Exception("Script file not found in classpath");
        }
        InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        engine.eval(reader);

        // Get the object from the script
        Object obj = engine.get("obj");
        // Check if the engine supports the Invocable interface
        if (engine instanceof Invocable) {
            Invocable invocable = (Invocable) engine;
            // ok: java_script_rule-ScriptInjection
            Object resultOne = invocable.invokeMethod(obj, "greetTwo", "World");
            // ruleid: java_script_rule-ScriptInjection
            Object resultTwo = invocable.invokeMethod(obj, "greetTwo", input);
            // Print the result
            System.out.println(resultOne);
            System.out.println(resultTwo);
        }
    }

    @Override
    public void run() throws Exception {
        System.out.println("Pass java.lang.Runtime.getRuntime().exec('/bin/sh ...'); as the user input to mimic a script injection");
        scriptingUnsafe("var x = 10; x * 2;");
        scriptingSafe();
        evalWithBindings("John", "Doe");
        invokeFunctionUsage("maliciousCommand");
        invokeMethodUsage("maliciousCommand");
    }
}

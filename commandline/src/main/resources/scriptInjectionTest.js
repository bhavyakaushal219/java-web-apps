function greetOne(name) {
    return "Hello, " + name + "!";
}

var obj = {
    greetTwo: function (name) {
        return 'Hello, ' + name;
    }
}
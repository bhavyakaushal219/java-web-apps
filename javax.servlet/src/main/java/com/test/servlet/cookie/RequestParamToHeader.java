package com.test.servlet.cookie;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;

// Try "Test\r\ning" as an input parameter if required (http://localhost:8080/ServletSample/RequestParamToHeader?input=Test\r\ning&type=a)
// Case 1: http://localhost:8080/ServletSample/RequestParamToHeader?input=safe&type=a
// Case 2: http://localhost:8080/ServletSample/RequestParamToHeader?input=safe&type=b
// Case 3: http://localhost:8080/ServletSample/RequestParamToHeader?input=safe&type=c
// Case 4: http://localhost:8080/ServletSample/RequestParamToHeader?input=safe&type=d

// ref: java_cookie_rule-RequestParamToHeader
public class RequestParamToHeader extends HttpServlet {

    public RequestParamToHeader() {

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String scenario = req.getParameter("type");
            String headerValue = "";
            switch (scenario) {
                case "a": {
                    headerValue = getParameterUsage(req);
                    // ruleid: java_cookie_rule-RequestParamToHeader
                    resp.addHeader("ADD_HEADER", headerValue);
                    break;
                }
                case "b": {
                    headerValue = getParameterNamesUsage(req);
                    // ruleid: java_cookie_rule-RequestParamToHeader
                    resp.setHeader("SET_HEADER", headerValue);
                    break;
                }
                case "c": {
                    headerValue = getParameterValuesUsage(req);
                    // ruleid: java_cookie_rule-RequestParamToHeader
                    resp.addHeader("ADD_HEADER", headerValue);
                    break;
                }
                case "d": {
                    headerValue = getParameterMapUsage(req);
                    // ruleid: java_cookie_rule-RequestParamToHeader
                    resp.setHeader("SET_HEADER", headerValue);
                    break;
                }
                default: {
                    headerValue = "Test\r\ning";
                    String escapedHeader = headerValue.replaceAll("[\\r\\n]+", "");
                    // ok: java_cookie_rule-RequestParamToHeader
                    resp.addHeader("ADD_HEADER", escapedHeader);
                }
            }
        } catch (Exception e) {
            resp.addHeader("Add-From-Param-Error", e.getMessage());
        }

        try (PrintWriter out = resp.getWriter()) {
            out.println("RequestParamToHeader Test: " + resp);
        }
    }


    public String getParameterUsage(HttpServletRequest req) {
        return req.getParameter("input");
    }

    public String getParameterNamesUsage(HttpServletRequest req) {
        Enumeration<String> parameterNames = req.getParameterNames();
        if (parameterNames.hasMoreElements()) {
            String firstParamName = parameterNames.nextElement();
            String[] firstParamValues = req.getParameterValues(firstParamName);
            return firstParamValues[0];
        }
        return "";
    }

    public String getParameterValuesUsage(HttpServletRequest req) {
        String[] parameterValues = req.getParameterValues("input");
        if (parameterValues != null && parameterValues.length > 0) {
            return parameterValues[0];
        }
        return "";
    }

    public String getParameterMapUsage(HttpServletRequest req) {
        Map<String, String[]> parameterMap = req.getParameterMap();
        if (!parameterMap.isEmpty()) {
            Map.Entry<String, String[]> firstEntry = parameterMap.entrySet().iterator().next();
            String firstParamName = firstEntry.getKey();
            String[] firstParamValues = firstEntry.getValue();
            return firstParamValues[0];
        }
        return "";
    }
}

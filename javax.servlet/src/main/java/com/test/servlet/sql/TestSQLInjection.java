package com.test.servlet.sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

// ref: java_inject_rule-CustomInjectionSQLString
// URL: http://localhost:8080/ServletSample/TestSQLInjection?name=Bob
@WebServlet(name = "TestSQLInjection", value = "/TestSQLInjection")
public class TestSQLInjection extends HttpServlet {

    private static final String URL = "jdbc:h2:file:/data/mydb";

    public TestSQLInjection() {
    }

    @Override
    public void init() {
        // Load H2 driver
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        createTable();
        System.out.println("1");
        insertData("Alice", "alice@example.com");
        insertData("Bob", "bob@example.com");
        System.out.println("2");
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name = request.getParameter("name");
        List<String> responses;
        if (name != null) {
            // Calling Log statements
            logStuff(name);
            responses = callVulnerableCodePatterns(name);
        } else {
            // for convenience
            // Calling Log statements
            logStuff("Alice");
            responses = callVulnerableCodePatterns("Alice");
        }

        // String str = queryData();
        if (responses != null) {
            response.getWriter().write(responses.toString());
        } else {
            response.getWriter().write("Something went wrong!");
        }
    }

    private Connection getConnection() throws SQLException {
        System.out.println("data connection");
        return DriverManager.getConnection(URL, "sa", "");
    }

    public void createTable() {
        String sql = "CREATE TABLE IF NOT EXISTS users (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), email VARCHAR(255))";

        try (Connection conn = getConnection(); Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
            System.out.println("Table created.");
            System.out.println("Table created.");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("exception table creation");
        }
    }

    public void insertData(String name, String email) {
        String sql = "INSERT INTO users (name, email) VALUES (?, ?)";

        try (Connection conn = getConnection(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.executeUpdate();
            System.out.println("Data inserted: " + name + ", " + email);
            System.out.println("Data inserted");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("exception Data inserted");
        }
    }

    public void insertDataUnsafely(String name, String email) {
        String sql = "INSERT INTO users (name, email) VALUES (?, ?)";

        try (Connection conn = getConnection(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.executeUpdate();
            System.out.println("Data inserted: " + name + ", " + email);
            System.out.println("Data inserted");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("exception Data inserted");
        }
    }

    public String queryData() {
        String sql = "SELECT * FROM users";
        StringBuilder sb = new StringBuilder();

        try (Connection conn = getConnection();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            while (rs.next()) {
                sb.append(rs.getInt("id") + ": " + rs.getString("name") + ", " + rs.getString("email"));

            }
            System.out.println(" Data queried");
        } catch (SQLException e) {
            sb.append(e.toString());

        }

        return sb.toString();
    }

    public String queryDataUnsafely(String query) {
        StringBuilder sb = new StringBuilder();

        try (Connection conn = getConnection();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                sb.append(rs.getInt("id") + ": " + rs.getString("name") + ", " + rs.getString("email"));
            }
            System.out.println(" Data queried");
        } catch (SQLException e) {
            sb.append(e.toString());
            System.out.println("exception Data query");
        }
        return sb.toString();
    }

    public List<String> callVulnerableCodePatterns(String input) {

        List<String> responses = new ArrayList<>();

        String sql;

        // Sample exploit :
        // http://localhost:8080/ServletSample/TestSQLInjection?name=Bob%27%20or%20id=%271

        // ruleid: java_inject_rule-CustomInjectionSQLString
        sql = "select id, name, email from users where name = '" + input + "'";
        responses.add(queryDataUnsafely(sql));

        // ok: java_inject_rule-CustomInjectionSQLString
        sql = "select id, name, email from users";
        responses.add(queryDataUnsafely(sql));

        // ruleid: java_inject_rule-CustomInjectionSQLString
        sql = String.format("Select * from users where name = '%s'", input);
        responses.add(queryDataUnsafely(sql));

        // ok: java_inject_rule-CustomInjectionSQLString
        sql = String.format("Select * from users");
        responses.add(queryDataUnsafely(sql));

        sql = "Select * from users";
        // ok: java_inject_rule-CustomInjectionSQLString
        sql += " where name = 'Alice'";
        responses.add(queryDataUnsafely(sql));

        sql = "Select * from users";
        // ruleid: java_inject_rule-CustomInjectionSQLString
        sql += " where name = '" + input + "'";
        responses.add(queryDataUnsafely(sql));

        // ruleid: java_inject_rule-CustomInjectionSQLString
        StringBuilder sb = new StringBuilder("select * from users where name = '" + input + "'");
        responses.add(queryDataUnsafely(sb.toString()));

        StringBuilder sb2 = new StringBuilder("select * from users ");
        // ruleid: java_inject_rule-CustomInjectionSQLString
        sb2.append("where name = '" + input + "'");
        responses.add(queryDataUnsafely(sb2.toString()));

        StringBuilder sb3 = new StringBuilder("select * from users ");
        // ok: java_inject_rule-CustomInjectionSQLString
        sb3.append("where name = 'Alice'");
        responses.add(queryDataUnsafely(sb3.toString()));

        StringBuilder sb4 = new StringBuilder();
        // ruleid: java_inject_rule-CustomInjectionSQLString
        sb4.append("select * from users where name = '" + input + "'");
        responses.add(queryDataUnsafely(sb4.toString()));

        // ok: java_inject_rule-CustomInjectionSQLString
        new StringBuilder("this is ").append(input).append(" totally unrelated.").toString();

        return responses;
    }

    public void logStuff(String input) {

        Logger log = Logger.getLogger(TestSQLInjection.class.getName());

        // ok: java_inject_rule-CustomInjectionSQLString
        String result2 = "insert into log file failed " + input;
        log.info(result2);

        // ok: java_inject_rule-CustomInjectionSQLString
        String result = "select user failed " + input;
        log.warning(result);

        // ok: java_inject_rule-CustomInjectionSQLString
        log.severe("select user failed " + input);

        // ok: java_inject_rule-CustomInjectionSQLString
        log.info(String.format("Select random Name %s", input));

    }
}

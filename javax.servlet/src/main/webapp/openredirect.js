document.addEventListener("DOMContentLoaded", function () {
  document.getElementById("triggerLink").addEventListener("click", function () {
    sendPostRequest();
  });
});

function sendPostRequest() {
  const endpoint =
    "http://localhost:8080/ServletSample/UnvalidatedRedirect?case=5";
  const jsonData = {
    url: "https://www.google.com",
  };

  var form = document.createElement("form");
  form.setAttribute("method", "post");
  form.setAttribute("action", endpoint);

  for (const key in jsonData) {
    if (jsonData.hasOwnProperty(key)) {
      var input = document.createElement("input");
      input.setAttribute("type", "hidden");
      input.setAttribute("name", key);
      input.setAttribute("value", jsonData[key]);

      form.appendChild(input);
    }
  }

  document.body.appendChild(form);
  form.submit();
}

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>

	<head>
		<title>Home</title>
	</head>

	<body>
		<h1>TEST: Spel Expression Parser</h1>
		<p style="width: 700px;"> The Spring Expression Language (SpEL for short) is a powerful expression
			language that supports querying and manipulating an object graph at runtime.
			The language syntax is similar to Unified EL but offers additional features, most notably method
			invocation and basic string templating functionality. </p>

		<form action="spel-vulnerable" method="post">
			<h4>Spel Expression Input</h4>
			<p style="width: 700px;"> Expression Parser that can be used to execute expressions within the
				standard evaluation context. </p>
			<input type="text" name="userName" width="200"><br>
			<input type="submit" value="Submit"><br><br>
			<label> Examples: </label><br>
			<label> 1. "John Doe"</label><br>
			<label> 2. "TEST".concat("!")</label><br>
			<label> 3. new String('hello world').toUpperCase()</label><br>
		</form><br>

		<form action="spel-svc" method="post">
			<h4>Spel Expression Input with Simple Evaluation Context</h4>
			<p style="width: 700px;"> This is an expression parser that uses the SimpleEvaluationContext which
				reduces the scope of expressions that can be parsed.
				However, direct user input is still processed as a simple expression, which means that an
				adversary could execute mathematical and logical expressions.
				This can trigger errors such as dividesByZero. </p>
			<input type="text" name="userName" width="200"><br>
			<input type="submit" value="Submit"><br><br>
			<label> Examples: </label><br>
			<h5>Following expressions will be evaluated</h5>
			<label> 1. "John Doe"</label><br>
			<label> 2. (12-12)/0</label><br>
			<label> 3. 1000.0^1000.0</label><br>
			<h5>Following expressions will not be evaluated</h5>
			<label> 4. "TEST".concat("!")</label><br>
			<label> 5. new String('hello world').toUpperCase()</label><br>
		</form>

	</body>

</html>
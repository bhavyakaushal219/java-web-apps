package com.gitlab.spring.controller;

import java.beans.Expression;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.SimpleEvaluationContext;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gitlab.spring.model.User;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class HomeController {

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		System.out.println("Home Page Requested, locale = " + locale);
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);
		
		return "home";
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public String user(@Validated User user, Model model) {
		System.out.println("User Page Requested");
		model.addAttribute("userName", user.getUserName());
		return "user";
	}

	@RequestMapping(value="/mvc", method=RequestMethod.GET)
	public ModelAndView mvc(HttpServletRequest request, HttpServletResponse response, Model model)  {
		// Create a look up table or pull from a data source
		HashMap<String, String> lookupTable = new HashMap<>();
		lookupTable.put("key1", "view1");
		lookupTable.put("key2", "view2");
		// Get user input
		String userInput = request.getParameter("key");
		// Look up view from the user input
		String viewValue = lookupTable.getOrDefault(userInput, userInput);
		// return the new model and view
		return new ModelAndView(viewValue);
	}



	@RequestMapping(value="/spel", method=RequestMethod.POST)
	public String spel(@Validated User user, Model model)  {
		// Create the Expression Parser
		SpelExpressionParser parser = new SpelExpressionParser();
		// Parse the expression
		org.springframework.expression.Expression parsedExpression = parser.parseExpression(user.getUserName());
		
		SimpleEvaluationContext context = SimpleEvaluationContext.forReadWriteDataBinding().build();
		Object result = parsedExpression.getValue(context);	
		user.setUserName(result.toString());
		model.addAttribute("userName", user.getUserName());
		System.out.println(result);
		return "user";
	}
}
